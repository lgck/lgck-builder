#include "./World.h"
#include "DebugDraw.h"
#include "../shared/stdafx.h"
#include "../shared/FrameSet.h"
#include "../shared/Frame.h"
#include "../shared/FileWrap.h"
#include "Shape.h"
#include <stdlib.h>
#include <Box2D/Box2D.h>
#include "ContactFilter.h"
#include "ContactListerner.h"
#include <cstdio>

CWorld::CWorld()
{
    m_joyState = 0;

    m_size = 0;
    m_max = 10;
    m_shapes = new CShape *[m_max];

    m_timeStep = 1.0f / 180.0f;
    m_velocityIterations = 8;
    m_positionIterations = 3;

    //create a new SFML rendering window
    m_window = new sf::RenderWindow(sf::VideoMode(800, 600, 32), "SFML Test");

    //prevent crazy frame rates (and make the framerate pseudo-sync with the physics stepping, though this is a crappy method of doing so)
    m_window->setVerticalSyncEnabled(true);

    //define the Box2D gravity vector
    b2Vec2 gravity = b2Vec2(0.f, 9.8f);
    m_world = new b2World(gravity);

    // init a ContactFilter class
    m_world->SetContactFilter(new CContactFilter());

    // add the contact listener to the world
    m_world->SetContactListener(new CContactListener());

    sf::Vector2u v = m_window->getSize();
    float width = v.x;
    float height = v.y;

    //create our debug drawing object and pass it to Box2D
    m_debugDraw = new DebugDraw(m_window);
    m_debugDraw->SetFlags(b2Draw::e_shapeBit);
    m_world->SetDebugDraw(m_debugDraw);

    CShape *shape;

    shape = new CShape(m_world, width/4.0f, height - 5.0f, width/2.0f, 5.0f, 0.0f, 0.3f);
    shape = new CShape(m_world, 100.0f, 200.0f, 100.0f, 100.0f);
    shape->setGroupIndex(3);

    shape = new CShape(m_world, 200.0f, 200.0f, 50.0f, 50.0f, 0.5f, 0.3f, 0.5f);
    shape->setGroupIndex(1);

    shape = new CShape(m_world, 300.0f, 200.0f, 50.0f, 50.0f, 0.5f, 0.9f, 0.1f);
    shape->setMask(2, 0x0002);
    shape->setGroupIndex(2);

    m_player = new CShape(m_world, 350.0f, 200.0f, 16.0f, 16.0f, 0.5f, 0.9f, 0.0f);
    m_player->setGroupIndex(4);
    m_player->setFixedRotation( true );

    loadRes();
}

CShape * CWorld::addShape(float x, float y, float w, float h, float density,float restitution,float friction)
{
    printf("%f %f %f %f\n", x,y,w,h);

    //define the static ground body, keeping the shapes from falling into oblivion
    b2BodyDef bodyDef;
    bodyDef.position.Set(x / PIXELS_PER_METER, y / PIXELS_PER_METER);
    if (density != 0) {
        bodyDef.type = b2_dynamicBody;
    } else {
        bodyDef.type = b2_staticBody;
    }

    printf("-->%f %f\n", x * METERS_PER_PIXEL, y * METERS_PER_PIXEL);

    //create body (static body)
    b2Body *body = m_world->CreateBody(&bodyDef);
    //create a polygon shape for the ground
    b2PolygonShape groundBox;

    // b2Vec2(0, 0)
    // set the origin as 0,0 so we can use these coordonates with SFML later
    b2Vec2 vec = b2Vec2(0.5f * w * METERS_PER_PIXEL, 0.5f * h*METERS_PER_PIXEL);
    groundBox.SetAsBox(0.5f * w * METERS_PER_PIXEL, 0.5f * h*METERS_PER_PIXEL, vec, 0);

    //fix the box shape to the body

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &groundBox;
    fixtureDef.density = density;
    fixtureDef.friction = friction;
    fixtureDef.restitution = restitution;
    body->CreateFixture(&fixtureDef);
    // recalculate the body mass after the density has changed
    body->ResetMassData();

    CShape *shape = new CShape(body);
    m_shapes[m_size] = shape;
    ++m_size;
    if (m_size==m_max) {
        m_max += GROWBY;
        CShape **t = new CShape* [m_max];
        memcpy(t, m_shapes, m_size * sizeof(CShape*));
        delete [] m_shapes;
        m_shapes = t;
    }

    return shape;
}

void CWorld::loadRes()
{
    const char s [] = "annie.obl";
    CFileWrap file;

    if (file.open(s, "rb")) {
        printf("file `%s` opened\n", s);

        CFrameSet images;
        if (images.extract(file)) {

            printf("images extracted: %d\n", images.getSize());
        }

        UINT8 * png;
        int size;
        images[4]->toPng(png, size);

        m_image.loadFromMemory(png, size);

        file.close();
    } else {
        printf("can't' open res\n");
    }
}

CWorld::~CWorld()
{
    delete m_window;
    delete m_world;
    delete m_debugDraw;
}

void CWorld::run()
{
    sf::Event sfmlEvent;
    //start the game loop
    while (m_window->isOpen()) {
            //check for window events
            while (m_window->pollEvent(sfmlEvent)) {

                if (sfmlEvent.type == sf::Event::Closed) {
                    m_window->close();
                }

                if (sfmlEvent.type == sf::Event::KeyPressed) {
                    if (sfmlEvent.key.code == sf::Keyboard::Escape) {
                        m_window->close();
                    }

                    if (sfmlEvent.key.code == sf::Keyboard::R) {
                        addShape(rand() % 500, 0.0f, 25.0f, 25.0f, rand() % 10 / 10.0f, rand() % 10 / 10.0f, rand() % 10 / 10.0f);
                    }

                    if (sfmlEvent.key.code == sf::Keyboard::U) {
                        addShape(rand() % 500, 0.0f, 25.0f, 25.0f);
                    }

                    if (sfmlEvent.key.code == sf::Keyboard::Y) {
                        CShape * shape = addShape(rand() % 500, 0, 25.0f, 25.0f, 0.3f);
                        shape->setGroupIndex(-1);
                    }

                    if (sfmlEvent.key.code == sf::Keyboard::Z) {
                        m_player->applyForce(b2Vec2 (0, -8));
                    }

                    if (sfmlEvent.key.code == sf::Keyboard::X) {
                        m_player->applyLinearImpulse(b2Vec2 (0, -8));
                    }

                }

                updateJoy(sfmlEvent);
                m_player->move(m_joyState);
            }

            //get the current "frame time" (seconds elapsed since the previous frame, hopefully close to 1/60 since vsync is enabled)
            //float frameTime = window->GetFrameTime();

            //step the Box2D physics world, with a constant time step
            //note that this is kind of a bad way to do time steps, as it can get out of sync if the framerate drops (see http://gafferongames.wordpress.com/game-physics/fix-your-timestep/ for more information)
            m_world->Step(m_timeStep, m_velocityIterations, m_positionIterations);
            m_world->ClearForces();

            //clear the window's rendering area before drawing
            m_window->clear();

            //have Box2D use our debug drawing class to render the world
            m_world->DrawDebugData();

            //update the rendering window with all the latest drawn items
            m_window->display();
    }
}

void CWorld::updateJoy(sf::Event event)
{
    switch (event.type) {
    case sf::Event::KeyPressed:
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
            m_joyState |= UP;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
            m_joyState |= RIGHT;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
            m_joyState |= DOWN;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
            m_joyState |= LEFT;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
            m_joyState |= JUMP;
        }
        break;

    case sf::Event::KeyReleased:
        if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            m_joyState &= MASK ^ UP;

        if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            m_joyState &= MASK ^ RIGHT;

        if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            m_joyState &= MASK ^ DOWN;

        if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            m_joyState &= MASK ^ LEFT;

        if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            m_joyState &= MASK ^ JUMP;
        break;
    }
}
