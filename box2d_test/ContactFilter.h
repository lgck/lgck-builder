#ifndef _CONTACT_FILTER_H
#define _CONTACT_FILTER_H

class b2ContactFilter;

class CContactFilter : public b2ContactFilter
{
	// There are more callbacks than this one, see manual
	bool ShouldCollide(b2Fixture* fixtureA, b2Fixture* fixtureB);
}; 

#endif
