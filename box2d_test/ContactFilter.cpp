#include <Box2D/Box2D.h>
#include "ContactFilter.h"

// add a method to test the collision data for filter settings
bool CContactFilter::ShouldCollide(b2Fixture* fixtureA, b2Fixture* fixtureB)
{
        const b2Filter filter1 = fixtureA->GetFilterData();
        const b2Filter filter2 = fixtureB->GetFilterData();

        // test to let pass or calculate collision results
        if(filter1.groupIndex == filter2.groupIndex) {
                return false;
        } else {
                return true;
        }
}
