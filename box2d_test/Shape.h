#ifndef __CSHAPE__H
#define __CSHAPE__H

#include <Box2D/Box2D.h>

class CShape
{
public:
    CShape(b2Body *);
    CShape(b2World *, float x, float y, float w, float h, float density=0,float restitution=0,float friction=0);
    ~CShape();

    void setMask(short categoryBits=1, short maskBits=0xffff );
    void setGroupIndex(int groupIndex);
    void setPosition(float x, float y);
    void getPosition(float &x, float & y);
    void move(int joyState);
    void setFixedRotation( bool set );
    void applyForce(const b2Vec2 & v);
    void applyLinearImpulse(const b2Vec2 & v );
    void setTransform( const b2Vec2 & v );

protected:
    b2Body * m_body;

    enum {
        GROWBY = 100,
        UP = 1,
        DOWN = 2,
        LEFT = 4,
        RIGHT = 8,
        JUMP = 16,
        MASK = 0xff
    };
};

#endif
