#ifndef _CONTACTLISTNER_H
#define _CONTACTLISTNER_H

class b2ContactListener;
class b2Contact;

/*
const int32 k_maxContactPoints = 2048;
struct ContactPoint
{
    b2Fixture* fixtureA;
    b2Fixture* fixtureB;
    b2Vec2 normal;
    b2Vec2 position;
    b2PointState state;
};
*/

class CContactListener : public b2ContactListener
{

public:
    void BeginContact(b2Contact* contact);
    void EndContact(b2Contact* contact);
    void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
    void PostSolve(b2Contact* contact);

protected:
    //int32 m_pointCount;
    //ContactPoint m_points[k_maxContactPoints];
};

#endif
