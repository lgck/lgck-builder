#ifndef FB__WORLD_H
#define FB__WORLD_H

#include <SFML/Graphics.hpp>

class b2World;
class b2Vec2;
class DebugDraw;
class CShape;

class CWorld
{
public:
    CWorld();
    ~CWorld();
    void run();

protected:

    b2World *m_world;
    CShape * m_player;
    //b2Vec2 m_gravity;
    float m_timeStep;
    int m_velocityIterations;
    int m_positionIterations;

    sf::RenderWindow *m_window;
    DebugDraw *m_debugDraw;

    void loadRes();
    sf::Image m_image;

    CShape* addShape(float x, float y, float w, float h, float density = 0.0f,float restitution = 0.0f,float friction = 0.0f);

    CShape** m_shapes;
    int m_size;
    int m_max;

    enum {
        GROWBY = 100,
        UP = 1,
        DOWN = 2,
        LEFT = 4,
        RIGHT = 8,
        JUMP = 16,
        MASK = 0xff
    };

    int m_joyState;

    void updateJoy(sf::Event event);

};

#endif
