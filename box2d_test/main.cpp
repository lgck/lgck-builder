#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include "zlib.h"
#include "../shared/stdafx.h"
#include "../shared/FileWrap.h"
#include "../shared/FrameSet.h"
#include <SFML/Graphics.hpp>
#include "World.h"
//mydefined headers

const int SCREEN_WIDTH = 900;
const int SCREEN_HEIGHT = 600;
const int SCREEN_BPP = 32;
const int gameTime = 10;
const int RATIO = 30;


int main(int argc, char* argv[], char* envp[])
{
    CWorld *world = new CWorld;
    world->run();

    return 0;
}
