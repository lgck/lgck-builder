#include "Shape.h"
#include "DebugDraw.h"
#include <stdio.h>

#define VEOLOCITY 10

CShape::CShape(b2Body * body)
{
    m_body = body;
}

CShape::CShape(b2World *world, float x, float y, float w, float h, float density,float restitution,float friction)
{
    //define the static ground body, keeping the shapes from falling into oblivion
    b2BodyDef bodyDef;
    bodyDef.position.Set(x / PIXELS_PER_METER, y / PIXELS_PER_METER);
    if (density != 0) {
        bodyDef.type = b2_dynamicBody;
    } else {
        bodyDef.type = b2_staticBody;
    }

    printf("-->%f %f\n", x * METERS_PER_PIXEL, y * METERS_PER_PIXEL);

    //create body (static body)
    b2Body *body = world->CreateBody(&bodyDef);
    //create a polygon shape for the ground
    b2PolygonShape groundBox;

    // b2Vec2(0, 0)
    // set the origin as 0,0 so we can use these coordonates with SFML later
    b2Vec2 vec = b2Vec2(0.5f * w * METERS_PER_PIXEL, 0.5f * h*METERS_PER_PIXEL);
    groundBox.SetAsBox(0.5f * w * METERS_PER_PIXEL, 0.5f * h*METERS_PER_PIXEL, vec, 0);

    //fix the box shape to the body

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &groundBox;
    fixtureDef.density = density;
    fixtureDef.friction = friction;
    fixtureDef.restitution = restitution;
    body->CreateFixture(&fixtureDef);
    // recalculate the body mass after the density has changed
    body->ResetMassData();

    m_body = body;

}

void CShape::setMask(short categoryBits, short maskBits)
{
    for (b2Fixture* fixture = m_body->GetFixtureList(); fixture; fixture = fixture->GetNext())  {
        //get the existing filter
         b2Filter filter = fixture->GetFilterData();

         //change whatever you need to, eg.
         filter.categoryBits = categoryBits;
         filter.maskBits = maskBits;
         //filter.groupIndex = ...;

         //and set it back
         fixture->SetFilterData(filter);
    }

    //    myFixtureDef.filter.categoryBits = categoryBits;
    //    myFixtureDef.filter.maskBits = maskBits;
}

void CShape::setGroupIndex(int groupIndex)
{
    for (b2Fixture* fixture = m_body->GetFixtureList(); fixture; fixture = fixture->GetNext())  {
         //get the existing filter
         b2Filter filter = fixture->GetFilterData();

         //change whatever you need to, eg.
         //filter.categoryBits = categoryBits;
         //filter.maskBits = maskBits;
         filter.groupIndex = groupIndex;

         //and set it back
         fixture->SetFilterData(filter);
    }
}

CShape::~CShape()
{

}

/*
void CShape::SetPosition(float x, float y) {
    xpos = x;
    ypos = y;
    //update position settings for Box2D

    playerBody.BOD->DestroyFixture(playerBody.BOD->GetFixtureList());
    world->DestroyBody(playerBody.BOD);
    playerBody.DEF.position.Set(xpos/RATIO, ypos/RATIO-1*5/RATIO);
    //playerBody.DEF.angularVelocity = 0;
    //playerBody.DEF.linearVelocity.Set(0, 60);
    playerBody.BOD = world->CreateBody(&playerBody.DEF);
    playerBody.BOD->CreateFixture(&playerBody.FIX);
    playerBody.RECT.setPosition(playerBody.BOD->GetPosition().x*RATIO,
                                playerBody.BOD->GetPosition().y*RATIO);
}

*/

void CShape::move(int joyState)
{
    float hy = 0;
    float hx = 0;

    if (joyState & RIGHT) {
        hx = 1;
        //m_body->SetLinearVelocity(b2Vec2(1, 0));
    }

    if (joyState & LEFT) {
        //m_body->SetLinearVelocity(b2Vec2(-1, 0));
        hx = -1;
    }

    if (joyState & UP) {
        hy = -1;
        //m_body->SetLinearVelocity(b2Vec2(1, 0));
    }

    if (joyState & DOWN) {
        //m_body->SetLinearVelocity(b2Vec2(-1, 0));
        hy = 1;
    }

    if (joyState & JUMP) {
        hy = -1;//VEOLOCITY;
    }
    m_body->SetLinearVelocity(b2Vec2(hx, hy));
    //m_body->ApplyLinearImpulse( b2Vec2(hx, hy), m_body->GetWorldCenter() );
}

void CShape::setFixedRotation( bool set )
{
    m_body->SetFixedRotation(true);
}

void CShape::applyForce(const b2Vec2 & v)
{
    m_body->ApplyForce( v , m_body->GetWorldCenter() );
}

void CShape::applyLinearImpulse(const b2Vec2 & v )
{
    m_body->ApplyLinearImpulse( v, m_body->GetWorldCenter() );
}

void CShape::setTransform(const b2Vec2 &v)
{
    m_body->SetTransform( b2Vec2(10,20), 0 );
}
